import { createRouter, createWebHistory } from 'vue-router';

// Pages
import Register from './pages/requests/PageUserRegister.vue';
import Login from './pages/requests/PageUserLogin.vue';
import LinkList from './pages/user/PageLinkList.vue';
import MainPage from './pages/user/PageMain.vue';
import Analytics from './pages/user/PageAnalytics.vue';
import PageBuilder from './pages/user/PageBuilder.vue';
import PageOnboarding from './pages/user/PageOnboarding.vue';
import PageProfileSettings from './pages/user/PageProfileSettings.vue';
import Reset from './pages/requests/PageResetPass.vue';
import FormReset from './pages/requests/PageFormResetPass.vue';
import ConfirmAccount from './pages/requests/PageConfirmAccount.vue';
import PageNewLink from './pages/user/PageNewLink.vue';
import PageFaq from './pages/other/PageFaq.vue';

// 404
import NotFound from './pages/NotFound.vue';
import HomePage from "./pages/requests/PageHomepage.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', component: HomePage, name: 'HomePage' },
    { path: '/register', component: Register, name: 'Register' },
    { path: '/login', component: Login, name: 'Login' },
    { path: '/reset', component: Reset, name: 'Reset' },
    { path: '/password-reset', component: FormReset, name: 'FormReset' },
    { path: '/confirm-account', component: ConfirmAccount, name: 'ConfirmAccount' },
    { path: '/links', component: LinkList, name: 'LinkList' },
    { path: '/faq', component: PageFaq, name: 'PageFaq' },
    {
      path: '/link/:id', component: MainPage, name: 'MainPage', children: [
        { path: 'onboarding', component: PageOnboarding, name: 'PageOnboarding' },
        { path: 'create-link', component: PageNewLink, name: 'PageNewLink' },
        { path: 'builder', component: PageBuilder, name: 'PageBuilder' },
        { path: 'analytics', component: Analytics, name: 'Analytics' },
        { path: 'settings', component: PageProfileSettings, name: 'PageProfileSettings' },
      ]
    },
    { path: '/:notFound(.*)', component: NotFound, name: 'NotFound' }
  ]
});

export default router;
