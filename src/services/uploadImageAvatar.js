import Api from './api.js';

const instance = {
  userAvatarUpload(linkId, file) {
    let formData = new FormData();

    formData.append("image", file);

    return Api.post(`/api/impact/link/${linkId}/image`, formData, {
      headers: {
        Content: "multipart/form-data"
      },
    });
  },
}

const UploadFilesService = Object.freeze(instance);

export default UploadFilesService;