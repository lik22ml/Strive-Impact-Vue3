import Api from './api.js';

const instance = {

  browsProduct(data) {
    return Api.post('/api/impact/products/browse', data);
  },

}

const ProductService = Object.freeze(instance);

export default ProductService;