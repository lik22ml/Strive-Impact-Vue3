import Api from './api.js';

const instance = {

  login(data) {
    return Api.post('/api/token', data);
  },

  register(data) {
    return Api.post('/api/register', data);
  },

  logOut() {
    return Api.delete('/api/token');
  },

  getLoggedUser() {
    return Api.get('/api/user');
  },

  usernameExists(username) {
    return Api.get(`/api/impact/link/exists?username=${username}`);
  },
}

const AuthService = Object.freeze(instance);

export default AuthService;