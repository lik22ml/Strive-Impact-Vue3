import Api from './api.js';

const instance = {

  requestNewPassword(data) {
    return Api.post(`/api/user/request-password-reset`, data);
  },

  setNewPassword(data) {
    return Api.post(`/api/user/set-password`, data);
  },

  resendEmailConfirmation(data) {
    return Api.post(`/api/user/resend-email-confirmation`, data);
  },

  confirmAccount(token) {
    return Api.post(`/api/user/confirm-account`, token);
  },

};

const ResetPassService = Object.freeze(instance);

export default ResetPassService;