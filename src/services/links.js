import Api from './api.js';

const instance = {

  createLink(data) {
    return Api.post('/api/impact/link', data);
  },

  updateLink(id, data) {
    return Api.put(`/api/impact/link/${id}`, data);
  },

  getLink(id) {
    return Api.get(`/api/impact/link/${id}`);
  },

  getLinkList() {
    return Api.get('/api/impact/link/list');
  },

}

const LinkService = Object.freeze(instance);

export default LinkService;