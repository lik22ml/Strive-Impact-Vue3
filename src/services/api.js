import axios from 'axios';
import router from '../router.js';
import { removeCookie } from "./deleteCookie.js";

// ENV
// @todo: this is hack to avoid proxy
let existingAuthToken = window.localStorage.getItem('Api-Token');

const bearerToken = process.env.VUE_APP_AUTH
const jsonApp = process.env.VUE_APP_JSON ?? 'application/json';
const mode = process.env.VUE_APP_MODE

const headersLocal = {
  Authorization: 'Bearer' + ' ' + (existingAuthToken ? existingAuthToken : bearerToken),
  Accept: jsonApp,
  Content: jsonApp
}
const headersProd = {
  Accept: jsonApp,
  Content: jsonApp
}

const Api = axios.create({
  baseURL: process.env.VUE_APP_BASE_ROUTE,
  timeout: 10000,
  withCredentials: true,
  headers: mode === 'development' ? headersLocal : headersProd,

});

const errorHandler = (error) => {
  if (error.response && error.response.status === 400) {
    // toast.error(error.response);
  } else if (error.response && error.response.status === 401) {
    // toast.error(error.response);
    router.push('/login');
    removeCookie('sanctum_token');
    window.localStorage.removeItem('Api-Token');
  } else if (error.response && error.response.status === 403) {
    router.push('/login');
    removeCookie('sanctum_token');
    window.localStorage.removeItem('Api-Token');
  } else if (error.response && error.response.status === 409) {
    // toast.error(error.response);
  } else if (error.response && error.response.status === 419) {
    // toast.error('Please refresh page' + error.response + ' ' + 'cfsr token error');
  } else if (error.response && error.response.status === 422) {
    // toast.error(`${error.response.data.errors}`, options);
  } else if (error.response && error.response.status === 500) {
    // toast.error('Server error, try again or contact admin');
  }

  return Promise.reject({
    ...error
  })
}

Api.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => errorHandler(error)
);

Object.freeze(Api);

export default Api;