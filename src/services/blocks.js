import Api from './api.js';

const instance = {

  create(id, data) {
    return Api.post(`/api/impact/link/${id}/block`, data);
  },

  updateBlock(id, data) {
    return Api.put(`/api/impact/block/${id}`, data);
  },

  deleteBlock(id, data) {
    return Api.delete(`/api/impact/block/${id}`, data);
  },

  getLinkBlockList(id) {
    return Api.get(`/api/impact/link/${id}/block/list`);
  },

  getBlock(id) {
    return Api.get(`/api/impact/block/${id}`);
  },

  updateLink(id) {
    return Api.get(`/api/impact/link/${id}`);
  },

  getLink(id) {
    return Api.get(`/api/impact/link/${id}`);
  },

}

const BlocksService = Object.freeze(instance);

export default BlocksService;