import { createApp } from 'vue'
import App from './App.vue'
import router from './router.js';
import Toast from "vue-toastification";
import Popper from "vue3-popper";
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";
// CSS
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/theme.min.css";
import "vue-toastification/dist/index.css";

// ICONS
import BootstrapIcon from '@dvuckovic/vue3-bootstrap-icons';

// Base components
import TheFooter from './components/layout/TheFooter.vue';
import TheHeader from './components/layout/TheHeader.vue';
import BaseSpinner from './components/ui/BaseSpinner.vue';
import BaseButton from './components/ui/BaseButton.vue';
import BaseTooltip from './components/ui/BaseTooltip.vue';

const app = createApp(App)

app.component("Popper", Popper);
app.component('BootstrapIcon', BootstrapIcon);
app.component('the-header', TheHeader);
app.component('the-footer', TheFooter);
app.component('base-spiner', BaseSpinner);
app.component('base-button', BaseButton);
app.component('base-tooltip', BaseTooltip);

const options = {
  position: "top-right",
  timeout: 4095,
  closeOnClick: true,
  draggable: true,
  pauseOnFocusLoss: true,
  pauseOnHover: true,
  draggablePercent: 1,
  showCloseButtonOnHover: false,
  closeButton: "button",
  icon: true,
  rtl: false,
  transition: "my-custom-fade",
  maxToasts: 5,
  newestOnTop: true
};

let envDomain = process.env.VUE_APP_SERVER_URL;
let envMode = process.env.VUE_APP_MODE;

Sentry.init({
  app,
  dsn: "https://bbc2c15e5af740de8cd12ecde8d06850@o549494.ingest.sentry.io/6132328",
  environment: envMode,
  integrations: [
    new Integrations.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
      tracingOrigins: [envDomain, /^\//],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

app.use(Toast, options);
app.use(router);


router.isReady().then(function () {
  app.mount('#app');
});
